/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.networkninjas.locale.helper;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

/**
 *
 * @author DnkCz <DnkCz networkninjas.cz>
 */
public class LocaleHelper {

    // language command-line parameter
    private static String commandLineLanguageParameter = "lang";

    // country command-line parameter
    private static String commandLineCountryParameter = "country";

    /*
    * path to Properties settings (aka default settings)
    * file placed in "src/bundles/settings.properties
     */
    private static String propertiesPath = "src/bundles/settings.properties";

    /* 
    * path to directory containing localized files..
    * aka String to directory:
    *      src/bundles/localization
    * containing:
    *      gui_en_US.properties
    *      gui_en_GB.properties
    * .. so on
    * containing files must be in format "somename_xx_YY.something"
    * where:
    * somename - file name
    * xx - is language according to ISO-639 Language Codes (aka en)
    * YY - is country according to ISO-3166 Country Codes (aka US)
    * something - suffix of file, preferably "properties"
     */
    private static String localizationDirSrc = "src/bundles/localization";

    // language String in PROPERTIES_BUNDLE file
    private static String propertiesBundleLanguageString = "lang";

    // country String in PROPERTIES_BUNDLE file
    private static String propertiesBundleCountryString = "country";

    // arguments passed to implementing java application
    private static String[] commandLineArguments;

    public static Locale getLocale() throws FileNotFoundException {
        Locale locale;

        // try to get locale from passed parameters to application
        locale = getLocaleFromCommandLineParams();
        if (locale != null) {
            return locale;
        }

        // try to get locale from passed properties file
        locale = getLocaleFromPropertiesFile();
        if (locale != null) {
            return locale;
        }

        // try to get locale from System properties
        locale = getSystemLocale();
        if (locale != null) {
            return locale;
        }

        return null;
    }

    public static Locale getLocaleFromCommandLineParams(String[] args) {
        LocaleHelper.setCommandLineArguments(args);
        return LocaleHelper.getLocaleFromCommandLineParams();
    }

    public static Locale getLocaleFromCommandLineParams() {
        Options options = new Options();
        options.addOption(commandLineLanguageParameter, true, "Language (en,cs...) according to ISO-639 Language Codes ");
        options.addOption(commandLineCountryParameter, true, "Country (US,CZ...) according to ISO-3166 Country Codes  ");

        DefaultParser defaultParser = new DefaultParser();
        CommandLine cmd;
        try {
            cmd = defaultParser.parse(options, LocaleHelper.commandLineArguments);

            String lang = "", country = "";
            if (cmd.hasOption(commandLineLanguageParameter)) {
                lang = cmd.getOptionValue(commandLineLanguageParameter);
            }

            if (cmd.hasOption(commandLineCountryParameter)) {
                country = cmd.getOptionValue(commandLineCountryParameter);
            }

            if (!lang.isEmpty() && !country.isEmpty()) {
                Locale locale = new Locale(lang, country);
                return checkIfSupportedOtherwiseReturnSameLanguageLocale(locale);
            }

        } catch (ParseException ex) {
            Logger.getLogger(LocaleHelper.class.getName()).log(Level.SEVERE, null, ex);

        }

        return null;
    }

    public static Locale getLocaleFromPropertiesFile(String propertiesPath) throws FileNotFoundException {
        LocaleHelper.propertiesPath = propertiesPath;
        return LocaleHelper.getLocaleFromPropertiesFile();
    }

    /*
    * Get Locale from properties
     */
    public static Locale getLocaleFromPropertiesFile() throws FileNotFoundException {
        Properties properties = new Properties();
        FileInputStream fs = new FileInputStream(new File(LocaleHelper.propertiesPath));
                
        if (!properties.containsKey(propertiesBundleLanguageString) || properties.containsKey(propertiesBundleCountryString)) {
            return null;
        }

        // check on locale just to be sure that files for locale in properties really exists
        Locale locale = new Locale(properties.getProperty(propertiesBundleLanguageString), properties.getProperty(propertiesBundleCountryString));
        return LocaleHelper.checkIfSupportedOtherwiseReturnSameLanguageLocale(locale);
    }


    /*
    * Try to get locale from System properties
     */
    public static Locale getSystemLocale() {
        String systemLang = System.getProperty("user.language");
        String systemCountry = System.getProperty("user.country");

        if (systemLang != null && systemCountry != null) {
            Locale locale = new Locale(systemLang, systemCountry);
            return LocaleHelper.checkIfSupportedOtherwiseReturnSameLanguageLocale(locale);
        }
        return null;
    }

    /* 
    * Check whether given locale is supported.
    * (aka check if localized files exists in LOCALIZATION_DIR_SRC)
    * output can be:
    *       "fullMatch" - indicates that locale as given exists 
    *       (aka given en_US and coresponding file somename_en_US.somesuffix exists)
    *       "langMatch" - indicates that locale as given NOT exists, but exists other locale with same language 
    *        (aka given en_US which DOES NOT exists but en_GB does exists)
     */
    public static Locale checkIfSupportedOtherwiseReturnSameLanguageLocale(Locale locale) {
        // check if exists exact locale, or same language
        File[] srcDirectoryFiles = new File(localizationDirSrc).listFiles();
        String tmpCountry = "";
        boolean fullMatch = false;
        for (File f : srcDirectoryFiles) {
            if (f.isFile()) {
                if (f.getName().contains("_" + locale.getLanguage() + "_" + locale.getCountry())) {
                    fullMatch = true;
                    break;
                }

                if (f.getName().contains("_" + locale.getLanguage())) {
                    // we take last part which is country code and save it to temporary variable
                    try {
                        tmpCountry = f.getName().split("_")[2];
                    } catch (ArrayIndexOutOfBoundsException e) {
                        tmpCountry = "";
                    }
                }
            }
        }

        if (fullMatch) {
            return locale;
        }
        if (!tmpCountry.isEmpty()) {
            return new Locale(locale.getLanguage(), tmpCountry);
        }
        return null;

    }

    public static void setCommandLineParameterLanguage(String lang) {
        LocaleHelper.commandLineLanguageParameter = lang;
    }

    public static void setCommandLineCountryParameter(String country) {
        LocaleHelper.commandLineCountryParameter = country;
    }

    public static void setLocalizationDirSrc(String settingsSrc) {
        LocaleHelper.localizationDirSrc = settingsSrc;
    }

    public static void setPropertiesBundleLanguageString(String language) {
        LocaleHelper.propertiesBundleLanguageString = language;
    }

    public static void setPropertiesBundleCountryString(String country) {
        LocaleHelper.propertiesBundleCountryString = country;
    }

    public static void setCommandLineArguments(String[] args) {
        LocaleHelper.commandLineArguments = args;
    }

    public static void setPropertiesPath(String propertiesPath) {
        LocaleHelper.propertiesPath = propertiesPath;
    }

}
