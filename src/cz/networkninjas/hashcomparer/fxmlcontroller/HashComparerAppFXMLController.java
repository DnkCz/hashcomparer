/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.networkninjas.hashcomparer.fxmlcontroller;

import cz.networkninjas.hashcomparer.core.HashComputer;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import sun.rmi.runtime.Log;

/**
 *
 * @author DnkCz networkninjas.cz
 */
public class HashComparerAppFXMLController implements Initializable {

    @FXML
    TextField src_txt, target_txt, sha256_hash, md5_hash;

    @FXML
    GridPane myRoot;

    @FXML
    Label sha256_hash_indicator, md5_hash_indicator;

    @FXML
    Label log_text;

    private ResourceBundle GUIBundle;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        GUIBundle = resources;
    }

    @FXML
    public void srcButtonAction() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("");
        File f = fileChooser.showOpenDialog(myRoot.getScene().getWindow());
        if (f != null) {
            src_txt.setText(f.getAbsolutePath());
        }
    }

    @FXML
    public void compare() {
        // set default values
        md5_hash_indicator.setText("?");
        sha256_hash_indicator.setText("?");
        md5_hash_indicator.setTextFill(Color.web("#000000"));
        sha256_hash_indicator.setTextFill(Color.web("#000000"));
        log_text.setText("");

        String path = src_txt.getText();
        File file = new File(path);
        if (!file.isFile()) {
            log_text.setTextFill(Color.web("#ff0000"));
            log_text.setText(GUIBundle.getString("error_file_not_exists"));
            return;
        } else {
            log_text.setTextFill(Color.web("#000000"));
            log_text.setText(GUIBundle.getString("info_file_found"));
        }

        HashComputer hashComputer = new HashComputer();
        hashComputer.setAlgorithm("MD5");
        hashComputer.setFile(file);
        try {
            // MD5
            byte[] hash = hashComputer.computeHash();
            md5_hash.setText(HashComputer.toHex(hash));

            // SHA-256
            hash = hashComputer.computeHash("SHA-256");
            sha256_hash.setText(HashComputer.toHex(hash));

            // comparation
            if (!target_txt.getText().isEmpty()) {
                // MD5
                if (target_txt.getText().toUpperCase().equals(md5_hash.getText().toUpperCase())) {
                    md5_hash_indicator.setText("OK");
                    md5_hash_indicator.setTextFill(Color.web("#00ff00"));
                } else {
                    md5_hash_indicator.setText("NO");
                }

                // SHA-256
                if (target_txt.getText().toUpperCase().equals(sha256_hash.getText().toUpperCase())) {
                    sha256_hash_indicator.setText("OK");
                    sha256_hash_indicator.setTextFill(Color.web("#00ff00"));
                } else {
                    sha256_hash_indicator.setText("NO");
                }
            }

        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(HashComparerAppFXMLController.class.getName()).log(Level.SEVERE, "Algorithm not supported", ex);
        } catch (IOException ex) {
            Logger.getLogger(HashComparerAppFXMLController.class.getName()).log(Level.SEVERE, "IOException", ex);
        } catch (HashComputer.NoAlgorithmSettedException ex) {
            Logger.getLogger(HashComparerAppFXMLController.class.getName()).log(Level.SEVERE, "Algorithm not setted", ex);
        }

    }

}
