/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.networkninjas.hashcomparer.core;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.DatatypeConverter;

/**
 *
 * @author Daniel
 */
public class HashComputer {

    private File file;
    private String algorithm;

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public String getAlgorithm() {
        return this.algorithm;
    }

    public void setAlgorithm(String algorithm) {
        this.algorithm = algorithm;
    }

    public byte[] computeHash(String algorithm) throws FileNotFoundException, NoSuchAlgorithmException, IOException, NoAlgorithmSettedException {
        this.setAlgorithm(algorithm);
        return computeHash();
    }

    public byte[] computeHash() throws FileNotFoundException, NoSuchAlgorithmException, IOException, NoAlgorithmSettedException {
        if (getAlgorithm() == null || getAlgorithm().isEmpty()) {
            throw new NoAlgorithmSettedException();
        }
        FileInputStream fileInputStream = new FileInputStream(getFile());
        MessageDigest messageDigest = MessageDigest.getInstance(getAlgorithm());
        byte[] buffer = new byte[1024 * 64];
        int numOfBytes;
        while ((numOfBytes = fileInputStream.read(buffer)) > 0) {
            messageDigest.update(buffer, 0, numOfBytes);
        }
        return messageDigest.digest();
    }

    // Converts an array of bytes into a string.
    public static String toHex(byte[] bytes) {
        return DatatypeConverter.printHexBinary(bytes);
    }

    public class NoAlgorithmSettedException extends Exception {

        // algorithm needs to be setted first
        public NoAlgorithmSettedException() {
            super();
        }
    }
}
