/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.networkninjas.hashcomparer.app;

import cz.networkninjas.hashcomparer.core.HashComputer;
import cz.networkninjas.locale.helper.LocaleHelper;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

/**
 *
 *
 * Application used to compare file hash with our hash.
 *
 * @author Daniel
 */
public class HashComparerApplication extends Application {

    private static String[] args;
    LocaleHelper localeHelper;

    protected static final String COMMAND_LINE_PARAMETER_LANGUAGE = "lang";
    protected static final String COMMAND_LINE_PARAMETER_COUNTRY = "country";
    protected static final String COMMAND_LINE_PARAMETER_RUN_AS_SCRIPT = "script";
    protected static final String COMMAND_LINE_PARAMETER_SRC = "src";
    protected static final String COMMAND_LINE_PARAMETER_TARGET = "target";

    protected static final String COMMAND_LINE_PARAMETER_LANGUAGE_DESC = "Language ISO code";
    protected static final String COMMAND_LINE_PARAMETER_COUNTRY_DESC = "Country ISO code";
    protected static final String COMMAND_LINE_PARAMETER_RUN_AS_SCRIPT_DESC = "Run as script parameter";
    protected static final String COMMAND_LINE_PARAMETER_SRC_DESC = "Path to file which hash will be calculated";
    protected static final String COMMAND_LINE_PARAMETER_TARGET_DESC = "Target hash which will be compared to calculated ones";

    // FIX paths
    private static final String LOCALIZATION_PATH = "src/cz/networkninjas/hashcomparer/bundles/localization";
    public static final String PROPERTIES_RELATIVE_PATH = "src/cz/networkninjas/hashcomparer/bundles/settings.properties";
    static final String gui_bundle_baseName = "cz.networkninjas.hashcomparer.bundles.localization.gui";

    private static String source;
    private static String destination;
    public static Locale locale;

    private static Properties properties;
    private static ResourceBundle GUIBundle;

    @Override
    public void init() throws IOException {
        Parameters parameters = getParameters();
        Locale locale;
        if (parameters != null) {
            localeHelper = new LocaleHelper();
            String[] args = parameters.getRaw().toArray(new String[0]);
            localeHelper.setCommandLineArguments(args);

            localeHelper.setPropertiesPath(PROPERTIES_RELATIVE_PATH);
            localeHelper.setLocalizationDirSrc(LOCALIZATION_PATH);
            locale = localeHelper.getLocale();
            GUIBundle = ResourceBundle.getBundle(gui_bundle_baseName, locale);
        }

        properties = new Properties();

        try {
            FileInputStream in = new FileInputStream(new File(PROPERTIES_RELATIVE_PATH));
            properties.load(in);
            in.close();
        } catch (IOException ex) {
            Logger.getLogger(HashComparerApplication.class.getName()
            ).log(Level.SEVERE, "Settings file " + PROPERTIES_RELATIVE_PATH + " does not exist.", ex);
        }
    }

    @Override
    public void start(Stage primaryStage) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/cz/networkninjas/hashcomparer/view/HashComparerAppFXML.fxml"));
            loader.setResources(GUIBundle);
            Parent root = loader.load();
            primaryStage.setTitle(properties.getProperty("app"));
            Scene scene = new Scene(root, 300, 275);
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (IOException ex) {
            Logger.getLogger(HashComparerApplication.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        if (args.length == 0) {
            launch(args);
            return;
        }

        // check if "run as script" parameter is present
        for (String arg : args) {
            if (arg.equals(COMMAND_LINE_PARAMETER_RUN_AS_SCRIPT)) {
                HashComparerConsoleApplication hashComparerConsoleApplication = new HashComparerConsoleApplication(args);
                hashComparerConsoleApplication.start();
                return;
            }
        }

    }

}
