/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.networkninjas.hashcomparer.core;

import java.io.File;
import java.io.FileNotFoundException;
import java.security.NoSuchAlgorithmException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;

/**
 *
 * @author DnkCz <DnkCz networkninjas.cz>
 */
public class HashComputerTest {

    final String SHA = "e7cf7d1853dfc30c1c44f571d3919eeeedef002823b66b6a988d27e919686389";
    final String PATH = "test\\files\\gradle-4.4.1-bin.zip";

    public HashComputerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of computeHash method, of class HashComputer.
     */
    // temporary disabled
    @Test
    public void testComputeHash_String() throws Exception {
        System.out.println("computeHash");
        HashComputer instance = new HashComputer();
        String algorithm = "SHA-256";
        instance.setFile(new File(this.PATH));
        byte[] result = instance.computeHash(algorithm);
        String hash = HashComputer.toHex(result);
        assertEquals(this.SHA.toUpperCase(), hash);
    }

    /**
     * Test of computeHash method, of class HashComputer.
     */
    @Ignore
    //@Test(timeout=1000)
    public void testComputeHash_0args() throws Exception {
        System.out.println("computeHash");
        HashComputer instance = new HashComputer();
        instance.setAlgorithm("SHA-256");
        File f = new File(this.PATH);
        instance.setFile(f);
        byte[] result = instance.computeHash();
        String hash = HashComputer.toHex(result);
        assertEquals(this.SHA.toUpperCase(), hash);
        // TODO review the generated test code and remove the default call to fail.
    }

    @Test(expected = FileNotFoundException.class)
    public void testComputeHash_0args_fail_file() throws Exception {
        System.out.println("computeHash");
        HashComputer instance = new HashComputer();
        instance.setAlgorithm("SHA-256");
        File f = new File(this.PATH);
        instance.setFile(new File("nonexisting"));
        byte[] result = instance.computeHash();
        String hash = HashComputer.toHex(result);
        assertEquals(this.SHA.toUpperCase(), hash);
        // TODO review the generated test code and remove the default call to fail.
    }

    @Test(expected = NoSuchAlgorithmException.class)
    public void testComputeHash_0args_fail_algorithm() throws Exception {
        System.out.println("computeHash");
        HashComputer instance = new HashComputer();
        instance.setAlgorithm("MyAlgorithm");
        File f = new File(this.PATH);
        instance.setFile(f);
        byte[] result = instance.computeHash();
        String hash = HashComputer.toHex(result);
        assertEquals(this.SHA, hash);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

}
